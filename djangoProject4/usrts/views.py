from django.shortcuts import render

# Create your views here.
from django.views import View

from usrts.models import Content, Category


class Categories(View):
    def get(self, request):
        return render(request, "main.html")


class FindCategory(View):
    def get(self, request):
        category = Content.objects.filter(category_id=request.categoty_id.id).first()

        return render(request, "category.html")


def category(request):
    return render(request, 'main.html', {'category': Category.objects.all()})


