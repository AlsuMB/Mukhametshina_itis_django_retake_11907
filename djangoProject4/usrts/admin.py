from django.contrib import admin

# Register your models here.
from usrts.models import Category,  Content


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['name']
        else:
            return []

@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    list_filter = ('category_id', )
    list_display = ['category_id', 'photo', 'text']

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['category_id', 'photo', 'text']
        else:
            return []
