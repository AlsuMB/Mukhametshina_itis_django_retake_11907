from . import views

from django.urls import path, include

from django.conf.urls import url


app_name = 'usrts'

urlpatterns = [
    path('category/', views.Categories.as_view(), name='Categories'),
    path('category/<int:category_id>/', views.FindCategory.as_view(), name='FindCategory'),
    path('', views.category, name='Categories'),
]
