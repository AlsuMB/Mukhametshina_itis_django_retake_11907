
from django.db import models


def user_photos_path(instance, filename):
    return f'category/category_{instance.category_id.id}/{filename}'


class Category(models.Model):
    name = models.CharField(max_length=150)


class Content(models.Model):
    category_id = models.ForeignKey('Category', on_delete=models.CASCADE)
    text = models.TextField()
    photo = models.ImageField(upload_to=user_photos_path, blank=True, null=True)
    views = models.IntegerField()